'''
this file is to log stats to influxdb
'''
from locust import runners
from influxdb import InfluxDBClient
from datetime import datetime
import json


class LocustInfluxdb(object):
    """
    this should be the connection between the test and the real-time logging into Influxdb
    """

    _db = None
    _settings = None
    _client = None
    _users = 0

    def create_db_settings(self, settings):
        """
        Here we create the connection, the db (if doesn't exist) and the client,
        through which we are going to write the stats into the DB
        :param settings: an object coming from the configuration (json)
        :return: None
        """
        self._settings = settings
        print self._settings

        self._client = InfluxDBClient(self._settings['host'], self._settings['port'], self._settings['username'],
                                      self._settings['username'], self._settings['database'])

        self._client.create_database(self._settings['database'])

    def hatch_complete(self, user_count):
        """
        on new user has been hatched,
        :param user_count: current number of users
        :return: None
        """
        self._users = user_count
        points = [{
                "measurement": "user_count",
                "time": datetime.utcnow().isoformat(),
                "fields": {
                    "value": user_count
                }
            }]
        self._client.write_points(points)

    def request_success(self, request_type, name, response_time, response_length):
        points = [{
                    "measurement": "request_success_duration",
                    "tags": {
                        "request_type": request_type,
                        "name": name
                    },
                    "time": datetime.utcnow().isoformat(),
                    "fields": {
                        "value": response_time,
                    }
                },
                {
                    "measurement": "request_success_response_time",
                    "time": datetime.utcnow().isoformat(),
                    "fields": {
                        "value": response_length
                    }
                }
            ]
        self._client.write_points(points)

    def request_failure(self, request_type, name, response_time, exception):
        points = [{
                    "measurement": "request_failure_duration",
                    "tags": {
                        "request_type": request_type,
                        "name": name
                    },
                    "time": datetime.utcnow().isoformat(),
                    "fields": {
                        "value": response_time
                    }
                },
                {
                    "measurement": "request_exception",
                    "time": datetime.utcnow().isoformat(),
                    "fields": {
                        "value": exception
                    }
                }
            ]
        self._client.write_points(points)

    def locust_error(self, locust_instance, exception, tb):
        points = [{
                    "measurement": "locust_instance_error",
                    "tags": {
                        "locust_instance": locust_instance
                    },
                    "time": datetime.utcnow().isoformat(),
                    "fields": {
                        "value": str(exception)
                    }
                }
            ]
        self._client.write_points(points)

    def locust_start_hatching(self):
        self._users = runners.locust_runner.user_count
        points = [{
            "measurement": "user_count",
            "time": datetime.utcnow().isoformat(),
            "fields": {
                "value": self._users
            }
        }]
        self._client.write_points(points)

    def locust_finished_tasks(self):
        self._users = runners.locust_runner.user_count
        points = [{
            "measurement": "user_count",
            "time": datetime.utcnow().isoformat(),
            "fields": {
                "value": self._users
            }
        }]
        self._client.write_points(points)

    def quitting(self):
        self._users -= 1
        points = [{
            "measurement": "user_count",
            "time": datetime.utcnow().isoformat(),
            "fields": {
                "value": self._users
            }
        }]
        self._client.write_points(points)

    def test_start(self, test_name):
        points = [
            {
                "measurement": "test_start_time",
                "tags": {
                    "test": test_name
                },
                "time": datetime.utcnow().isoformat(),
                "fields": {
                    "value": datetime.utcnow().isoformat()
                }
            }
        ]
        self._client.write_points(points)