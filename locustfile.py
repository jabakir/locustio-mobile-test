#!/usr/bin/python

from locust import HttpLocust, events
from users import user, user_control
from conf import default_conf
from user_behaviour.task_factory import Journey
from stats.real_time_stats import LocustInfluxdb


#reporter_settings = default_conf.default_conf.load_json_configuration_from_file('conf/influxdb_stats_conf.json')
#print reporter_settings
#reporter = LocustInfluxdb()
#reporter.create_db_settings(reporter_settings)

"""
get the default configuration based on cli arguments or any other os environment variable
and set user list and tasks accordingly. 
Here should be all the configuration settings got for reporting and stats and DB connection.
"""
CONF = default_conf.default_conf.get_default_test_values()

Journey.set_conf_file('conf/rebrush_part_conf.json')
Journey.set_users_conf_file('users/user_credentials_vikings.json')


class MobileUser(HttpLocust):
    task_set = user.SequentialTaskSet


"""
EVENT TRAPPING
"""
#events.request_success += reporter.request_success
#events.hatch_complete += reporter.hatch_complete
#events.request_failure += reporter.request_failure
#events.locust_error += reporter.locust_error
#events.quitting += reporter.quitting
#events.locust_start_hatching += reporter.locust_start_hatching

#events.locust_finished_tasks = events.EventHook()
#events.locust_finished_tasks += reporter.locust_finished_tasks

"""
CUSTOM EVENT TRAPPING
"""
# events.start_test = events.EventHook()
# events.start_test += reporter.test_start
# events.start_test.fire(test_name='rebrush_test')

# custom event trapping -- result set ids
# events.result_ids_set_bet = events.EventHook()
# events.result_ids_set_bet += reporter.result_ids_set_bet

raw, tss = user_control.parse_users_in_time_data('users/data/users_overall.csv', 1)
user_control.start_user_control(raw, tss[0]/2, 10)

