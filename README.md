# Tipico performance tests (Locust.io)

## Structure

### Configuration
The project contains three major sections:

* configuration
* task generation
* user flow

The user flow relies on the json configuration file in the `conf` directory. The locustfile (as the main file loaded 
into the locust application) sets these paths for the configuration loader/parser class, which can be then passed 
through the task generator and user behaviour classes. 

The `conf/default_conf.py` file has all cli option check from the main locustio application, where we are able to parse
and modify our implementation and user flow based on the cli options.

### Task generation
The tasks for the user flow are generated in the two singleton classes in the `user_behaviour/task_factory.py` file.

### User flow
User is generally the class, which executes the tasks in the performance test. The `SequentialTaskSet` class is a 
subclass of the TaskSet locust class and modified to be able to execute the task list in sequence instead of executing
in random order, as it is the default behaviour of the locust.io application.

