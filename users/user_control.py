'''
the user count and hatching control.
the purpose is to being able to mimic real-life scenarios,
focusing on user concurrency on the environment
'''
import csv, sys
from locust import runners
from threading import Thread
import time
from locust.stats import RequestStats


def noop(*arg, **kwargs):
    '''
    Monkey patch to be able to prevent resetting stats while new locusts are being hatched
    :param arg:
    :param kwargs:
    :return:
    '''
    print "Stats reset prevented by monkey patch!"


# setting the monkey patch
RequestStats.reset_all = noop


def parse_users_in_time_data(data_source, steps=1):
    '''
    parse a csv file with timestamps and user counts
    :param data_source: path to the csv file
    :return: a dict with key:value pairs (timestamp:)
    '''
    with open(data_source, 'r') as users:
        reader = csv.DictReader(users)

        inp = [{'ts': row['timestamp'], 'uc': row['user_count']} for row in reader]
        val = [int(inp[y]['ts']) - int(inp[x]['ts']) for x, y in zip(range(1, len(inp), steps), range(1+steps, len(inp), steps))]
        return inp, val


def normalize_concurrency(interval=6000):
    '''
    ---don't use - far not used
    :param interval:
    :return:
    '''
    print 'default bucket size: ' + str(interval) + 'ms'
    pass


class options(object):
    '''
    this object should be passed to the locust runner, when more locusts needs to be hatched
    '''
    hatch_rate = 1
    num_clients = 0
    num_requests = 0
    no_reset_stats = 1
    timeout = 0


class BackgroundTimer(Thread):
    '''
    Using a separate, non-blocking thread, new locust hatching can be scheduled
    '''
    def __init__(self, opts):
        self.opts = opts
        Thread.__init__(self, None, None, 'scheduler')

    def run(self):
        while 1:
            time.sleep(self.opts.timeout)
            if len(self.opts.points) > 0:
                self.scheduled_job()
            else:
                sys.exit(0)

    def kill_scheduler(self):
        pass

    def scheduled_job(self):
        new_clients = self.opts.points.pop(0)
        num_clients = int(new_clients['uc'])
        print 'CURRENT LOCUSTS ATTACKING... : ' + str(num_clients/8)
        runners.locust_runner.start_hatching(locust_count=int(num_clients/8), hatch_rate=self.opts.hatch_rate)


def start_user_control(points, interval, hatch_rate):
    opts = options()
    opts.hatch_rate = hatch_rate
    opts.num_clients = 1
    opts.timeout = interval/1000
    opts.points = points

    timer = BackgroundTimer(opts)
    timer.start()











