"""
this is the User class with behaviour/tasks, what has to execute.

"""
from locust import runners, events
import json
from locust import TaskSet
from user_behaviour.task_factory import TaskFactory, Journey


# TODO: this class should be reffactored and create a base class with option
# to be able to choose sequential or non sequential taskset


class SequentialTaskSet(TaskSet):

    tasks = []

    task_counter = -1
    path_to_parse = None
    parser = None

    def __init__(self, parent):
        """
        The __init__ is overwritten to be able to create a specific user journey
        for each user and control it per user.
        """
        events.locust_start_hatching.fire()

        TaskSet.__init__(self, parent)
        # here we can check for user props or set them
        self.create_journey(Journey.get_task_list())

    def create_journey(self, task_list):
        """
        To create a user journey the parsed config json is passed into the method
        :param task_list - the parsed json
        """
        self.min_wait = task_list['min_wait']
        self.max_wait = task_list['max_wait']

        if 'result_id_path' in task_list.keys() and len(task_list['result_id_path']) > 0:
            self.path_to_parse = task_list['result_id_path']

        task_to_add = list(task_list['task_list'])
        task_to_add.reverse()
        # print task_to_add

        cntr = 0

        while len(task_to_add) > 0:
            cntr += 1
            task = task_to_add.pop(-1)

            # check if there is path to parse a result matching the current tasks's path property
            if self.path_to_parse == task['path']:
                self.parser = (self.parse_result_ids_from_array_of_arrays, self.path_to_parse)
            else:
                self.parser = None

            #  based on the task type property, create a GET/POST task accordingly
            if task['type'] == "GET":
                self.tasks.append(TaskFactory.get_request(task['path'], headers=task['headers'], parser=self.parser, weight=task['weight']))
            elif task['type'] == "POST":
                self.tasks.append(TaskFactory.post_request(task['path'], data=task['data'], headers=task['headers'], payload=task['payload'], parser=self.parser, weight=task['weight']))

    def get_next_task(self):
        """
        This overwritten method makes the whole thing sequential.
        """
        if self.task_counter < 0:
            task = self.tasks[0]
            self.task_counter = 0
        else:
            task = self.tasks[self.task_counter]

        if self.task_counter < len(self.tasks) - 1:
            self.task_counter += 1
        else:
            self.task_counter = 0
            self.on_complete()
        return task


    def on_complete(self):
        events.locust_finished_tasks.fire()

    def parse_result_ids_from_array_of_arrays(self, json_obj):
        """
        this is to parse result ids from the array of arrays
        :param json_obj:
        :return:
        """
        raw_json = json.loads(json_obj)
        resids = [arr[1] for arr in raw_json[2:] if type(arr) is list and 'match' in arr]
        TaskFactory.set_result_ids(resids)


