"""
this class is to generate HttpLocust sessions dynamically and the resulting
functions would be added to the TaskSet.
"""
from conf import default_conf
import random, json, math
from locust import events, task

# TODO: put all $require_* json variables in a separate class and create a utility for that
# so far having:
#   a) $require_user_credentials
#   b) $require_result_ids

# TODO: add weight to different tasks to be able to modify the journey per user. For that,
# using a sub-taskset would be ideal


class TaskFactory(object):
    """
    singleton utility class to generate locust task functions, which
    would be added to the user's task list
    """

    result_ids = []

    @classmethod
    def set_result_ids(cls, res_ids):
        # print res_ids
        cls.result_ids = res_ids

    @classmethod
    def get_result_ids(cls):
        return cls.result_ids

    @classmethod
    def get_request(cls, path, headers, parser, weight):
        """
        :param path: the requested path - str
        :param headers: json object to pass to the request
        :param parser: a tuple with parser function and the path where it should parse the Result
        :param hook: a possible hook
        :return: the GET task function
        """
        @task(weight=weight)
        def req(locust):
            own_path = path
            if '$random_event_id' in own_path:
                own_path = '/ajax/sports/event/' + str(TaskFactory.get_random_result_ids()[0]) + '/favorites'
            with locust.client.get(own_path, headers=headers) as catched:
                if parser and catched.status_code == 200:
                    parser[0](catched._content)
                # if hook:
                #     print hook

        return req

    @classmethod
    def post_request(cls, path, data, headers, payload, parser, weight):
        """
        :param path: the requested path - str
        :param data: json object to pass to the request
        :param headers: json object to pass to the request
        :param payload: json object to pass to the request
        :param hook: a possible hook
        :param parser: parser: a tuple with parser function and the path where it should parse the Result
        :return: the POST task function
        """

        @task(weight=weight)
        def req(locust):
            own_headers = headers
            own_payload = payload
            own_data = data

            if own_data == '$require_user_credentials':
                creds = Journey.get_users_creds_list()
                own_data = creds[int(math.floor(random.random() * len(creds)))]

            if len(TaskFactory.get_result_ids()) > 0 and own_payload == '$require_result_ids':
                ids = TaskFactory.get_random_result_ids()
                result = locust.client.post(path, data=own_data, headers=own_headers, json=ids)
            else:
                result = locust.client.post(path, data=own_data, headers=own_headers, json=payload)

            with result as catched:
                if parser and catched.status_code == 200:
                    parser[0](catched._content)
                # if hook:
                #     print hook

        return req

    @classmethod
    def get_random_result_ids(cls):
        """
        Get random number and random result ids for bet placement
        :return: list of result ids
        """
        ids = set()
        r_amount = int(1 + round(random.random() * 4))

        for cnt in range(0, 1):
            ids.add(cls.result_ids[int(round(random.random() * (len(cls.result_ids) - 1), 0))])
            # print r_amount
            # print ids

        # print list(ids)
        return list(ids)


class Journey(object):
    """
    a singleton utility class to create and store the user journey
    """
    _task_list = None
    _users_list = None
    _conf_file = None
    _users_conf_file = None
    _jsessionid = None

    @classmethod
    def get_task_list(cls):
        return cls._task_list

    @classmethod
    def get_users_creds_list(cls):
        return cls._users_list

    @classmethod
    def create_task_list(cls):
        cls._task_list = default_conf.default_conf.load_json_configuration_from_file(cls._conf_file)

    @classmethod
    def create_users_credentials_list(cls):
        cls._users_list = default_conf.default_conf.load_json_configuration_from_file(cls._users_conf_file)

    @classmethod
    def set_conf_file(cls, conf_path):
        cls._conf_file = conf_path
        cls.create_task_list()

    @classmethod
    def set_users_conf_file(cls, conf_path):
        cls._users_conf_file = conf_path
        cls.create_users_credentials_list()

    @classmethod
    def set_jsessionid(cls, id):
        cls._jsessionid = id

    @classmethod
    def get_jsessionid(cls):
        return cls._jsessionid

